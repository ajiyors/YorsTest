//Apiary Test Mock Server
const HOST = "https://private-a62663-yorstest.apiary-mock.com";

function checkNetStatus(response) {
    if (!response.ok) {
        const error = new Error(response.statusText, -1);
        error.response = response;
        throw error;
    }
    return response.json();
}


function checkResultStatus(result) {
    return result;
}

export function fetchHelper(url, method, args = {}, contentType = 'form') {
    let mheaders = {};
    if (contentType == 'form') {
        mheaders = {
            Accept: 'application/x-www-form-urlencoded',
            'Content-Type': 'application/x-www-form-urlencoded',
        }
    } else if (contentType == 'json') {
        mheaders = {
            Accept: 'application/x-www-form-urlencoded',
            'Content-Type': 'application/json; charset=UTF-8',
        }
    }

    let furl = `${HOST}${url}`;

    switch (method.toLowerCase()) {
        case 'post':
        case 'put':
        case 'delete':
            return timeout(20000,
                fetch(furl, {
                    method: method,
                    headers: mheaders,
                    body: JSON.stringify(args),
                    cache: 'default',
                }).then(checkNetStatus).then(checkResultStatus)
            )
        case 'get':
            return timeout(20000,
                fetch(furl, {
                    method: method,
                    headers: mheaders
                })).then(checkNetStatus).then(checkResultStatus)

    }
}

function timeout(ms, promise) {
    return new Promise((resolve, reject) => {
        const timeoutId = setTimeout(() => {
            reject(new Error("timeout"))
        }, ms);
        promise.then(
            (res) => {
                clearTimeout(timeoutId);
                resolve(res);
            },
            (err) => {
                clearTimeout(timeoutId);
                reject(err);
            }
        );
    })
}